from Model.FilePath import *
from Model.Document import *
import sys

sys.stdout = open('log.txt', 'w')
path = FilePath()
d = Document("suspicious","suspicious-document00004.txt")

#print(str(len(d.line)))
#print(d.line)
#print(str(len(d.text)))
#print(d.text)

d.mappingCharacter()
d.removeNonAlphanumericCharacter()

d.parseIndexMap()
d.removeStopwords()

d.generateFeature()

#print(d.cleanData)
