import csv
import os
import re

from nltk.corpus import stopwords

from Model.FilePath import *

class Document:

    def __init__(self,type,docName):
        self.name = docName
        self.type = type

        path = FilePath()

        file = open(path.type[type] + docName, encoding="utf8")

        self.line = file.readlines()

        self.mergeText()

    def mergeText(self):
        self.text = ""

        for item in self.line:
            self.text = self.text + item

        self.text = self.text.replace("\n", " ") #Remove White Space
        self.text = self.text.lower() #Lower Casing

    def mappingCharacter(self):
        self.indexMap = []

        for index in range(0, len(self.text)):
            self.indexMap.append([index, self.text[index]])

    def removeNonAlphanumericCharacter(self):
        nonAlphanumericCharacter = "`~!@#$%^&*()_+-={}|[]\:\"<>?;',./'"
        result = []

        for index in range(0, len(self.indexMap)):
            flag = True

            for item in nonAlphanumericCharacter:
                if self.indexMap[index][1] == item:
                    flag = False
                    break

            if flag == True:
                result.append(self.indexMap[index])

        self.indexMap = result

    def parseIndexMap(self):

        itt = 0
        temp = []

        while True:
            #print(itt)
            word = ""
            offset = 0

            if itt > len(self.indexMap):
                break

            if self.indexMap[itt][1] != ' ':
                itt2 = itt+1
                word = word + self.indexMap[itt][1]
                offset = self.indexMap[itt][0]

                while True:
                    #print(itt2)
                    #print(word)
                    if itt2 >= len(self.indexMap):
                        break
                    else:
                        if self.indexMap[itt2][1] == ' ':
                            break
                        else:
                            word = word + self.indexMap[itt2][1]

                        itt2 = itt2+1

            itt = itt2+1

            temp.append([offset, word])

        self.cleanData = temp

    def removeStopwords(self):
        listStopwords = stopwords.words("english")

        result = []

        for item in self.cleanData:
            flag = True

            for item2 in listStopwords:
                if item2 == item[1]:
                    flag = False
                    break

            if flag == True:
                result.append(item)


        self.cleanData = result

    nF = 4
    def generateFeature(self):
        featuresTable = []

        self.tableFeature = self.cleanData

        for i in range(0, len(self.tableFeature)):
            for j in range(0, self.nF):
                if i - j <= 0:
                    self.tableFeature[i].append(['*', self.tableFeature[i][1]])
                else:
                    self.tableFeature[i].append([self.tableFeature[i - j - 1][1], self.tableFeature[i][1]])

        for item in self.tableFeature:
            print(item)
