class FilePath:
    def __init__(self):
        self.type = {}

        self.datasetPath = "../Dataset/"

        self.sourcePath = self.datasetPath + "src/"
        self.sourceType = "source"
        self.type["source"] = self.datasetPath + "src/"

        self.suspiciousPath = self. datasetPath + "susp/"
        self.suspiciousType = "suspicious"
        self.type["suspicious"] = self. datasetPath + "susp/"

        self.noPlagiarismPath = self.datasetPath + "01-no-plagiarism/"
        self.noPlagiarismType = "no-plagiarism"

        self.noObfuscationPath = self.datasetPath + "02-no-obfuscation/"
        self.noObfuscationType = "no-obfuscation"

        self.randomObfuscationPath = self.datasetPath + "03-random-obfuscation/"
        self.randomObfuscationType = "random-obfuscation"

        self.translationObfuscationPath = self.datasetPath + "04-translation-obfuscation/"
        self.translationObfuscationType = "translation-obfuscation"

        self.summaryObfuscationPath = self.datasetPath + "05-summary-obfuscation/"
        self.summaryObfuscationType = "summary-obfuscation"

        self. pairsFileDir = self.datasetPath + "pairs"